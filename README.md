# Application Migration Testing

This repo is meant to be a public log of current media and entertainment software testing on RHEL 8, RHEL 9, and compatible platforms as the industry begins the shift away from RHEL 7. This is separate from vendors actually building or certifying their sofware for the newer platforms.

Issues should be made to track discovered issues, with the wiki serving as a more "final" user-facing guide for the current state of support.
